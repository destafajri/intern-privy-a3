# Intern Privy-A3



## Here's my resource of task A3

1. Golang Mux API https://gitlab.com/destafajri/intern-privy-a3-golangmuxapi
2. Golang Gin Web API https://gitlab.com/destafajri/intern-privy-a3-golang-web-api-gin-library
3. Golang MongoDB https://gitlab.com/destafajri/intern-privy-a3-golang-mongodb
4. Golang MongoDB Atlas https://gitlab.com/destafajri/intern-privy-a3-golang-mongodb-atlas
5. Postman Automation https://gitlab.com/destafajri/intern-privy-a3-postman-automation